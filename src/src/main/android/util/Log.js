/* eslint-disable no-dupe-class-members */
/* eslint-disable no-console */
class Log {
  static v(tag, msg) {
    console.log(tag, msg)
  }

  static v(tag, msg, tr) {
    console.log(tag, msg, tr)
  }

  static d(tag, msg) {
    console.log(tag, msg)
  }

  static d(tag, msg, tr) {
    console.log(tag, msg, tr)
  }

  static i(tag, msg) {
    console.info(tag, msg)
  }

  static i(tag, msg, tr) {
    console.info(tag, msg, tr)
  }

  static w(tag, msg) {
    console.warn(tag, msg)
  }

  static w(tag, msg, tr) {
    console.warn(tag, msg, tr)
  }

  // eslint-disable-next-line no-unused-vars
  static isLoggable(var0, var1) {
    return true
  }

  static w(tag, tr) {
    console.warn(tag, tr)
  }

  static e(tag, msg) {
    console.error(tag, msg)
  }

  static e(tag, msg, tr) {
    console.error(tag, msg, tr)
  }

  static wtf(tag, msg) {
    console.log(tag, msg)
  }

  static wtf(tag, tr) {
    console.log(tag, tr)
  }

  static wtf(tag, msg, tr) {
    console.log(tag, msg, tr)
  }


  static getStackTraceString(tr) {
    console.trace(tr)
  }

  static println(priority, tag, msg) {
    console.log(priority, tag, msg)
  }
}

Log.ASSERT = 7
Log.DEBUG = 3
Log.ERROR = 6
Log.INFO = 4
Log.VERBOSE = 2
Log.WARN = 5
export default Log
