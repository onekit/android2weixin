import inputmethod from './inputmethod/index'
import View from './View'

module.exports = {
  inputmethod, View
}
