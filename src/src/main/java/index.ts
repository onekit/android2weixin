import io from './io/index'
import lang from './lang/index'

module.exports = {
  io,
  lang
}
