import activity from './activity/index'
import appcompat from './appcompat/index'
import core from './core/index'
import fragment from './fragment/index'

module.exports = {
  activity, appcompat, core, fragment
}
