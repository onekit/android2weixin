import android2weixin from '../../../../index';
const Intent = android2weixin.android.content.Intent
const AppCompatActivity = android2weixin.androidx.appcompat.app.AppCompatActivity
const R = android2weixin.demo.layout.R
export default class MainActivity extends AppCompatActivity {

      onCreate( savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
     menu_click( sender){
        switch (sender.getId()){
            case R.id.linearLayoutButton:
                startActivity(new Intent(this,LinearLayoutDemo.class));
                break;
        }
   }
}
